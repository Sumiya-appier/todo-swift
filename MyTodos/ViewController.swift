import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Todo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let TodoCell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "TodoCell", for: indexPath)
        TodoCell.textLabel!.text = Todo[indexPath.row]
        return TodoCell
    }
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // To check added todos
        if UserDefaults.standard.object(forKey:"TodoList") != nil{
        Todo = UserDefaults.standard.object(forKey:"TodoList") as! [String]
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

