import UIKit
var Todo = [String]()

class AddController: UIViewController {
    // Tag to Text field
    @IBOutlet weak var TodoTextField: UITextField!
    
    // Tag to Add button and event
    @IBAction func TodoAddButton(_ sender: Any) {
        Todo.append(TodoTextField.text!)
        TodoTextField.text = ""
        UserDefaults.standard.set(Todo, forKey: "TodoList")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

}
